import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Category} from "src/app/model/category";
import {Event} from "./model/event";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  baseURL = "/api/category";

  constructor(private httpClient: HttpClient) {
  }

  saveCategory(category: Category): Observable<Category> {
    return this.httpClient.post<Category>(`${this.baseURL}`, category);
  }


  readCategory(categoryId: number): Observable<Category> {
    return this.httpClient.get<Category>(`${this.baseURL}/${categoryId}`);

  }

  readCategories(): Observable<Category[]> {
    return this.httpClient.get<Category[]>(`${this.baseURL}`)

  }

  updateCategory(categoryId: number, categoryToUpdate: Category): Observable<Category> {
    return this.httpClient.put<Category>(`${this.baseURL}/${categoryId}`, categoryToUpdate)
  }

  deleteCategory(categoryId: number): Observable<any> {
    return this.httpClient.delete<any>(`${this.baseURL}/${categoryId}`);

  }

  getEventsOfCategory(categoryId: number): Observable<Event[]> {
    return this.httpClient.get<Event[]>(`${this.baseURL}/eventsOf/${categoryId}`)
  }
}
