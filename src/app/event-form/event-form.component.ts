import {Component,Input} from '@angular/core';
import {Event} from "src/app/model/event";
import {FormControl, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {EventService} from "../event.service";
import {Category} from "../model/category";
import {CategoryService} from "../category.service";

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.css']
})
export class EventFormComponent {


  constructor (private eventService:EventService,private categoryService:CategoryService,
               private router:Router){

  }

  categories: Category []=[];

  @Input()
  event: Event = {
    id: null,
    name: "",
    description: "",
    location: "",
    startDate: null,
    endDate: null,
    imgURL: null,
    category:null,
  }

  // Event form-ul acesta este un container pt toate casutele de tip input !!!
  eventForm: FormGroup = new FormGroup({
    nameInput: new FormControl(),
    descriptionInput: new FormControl(),
    locationInput: new FormControl(),
    imgURLInput: new FormControl(),
    startDateInput: new FormControl(),
    endDateInput: new FormControl(),
    categoryInput: new FormControl(),
  });

  ngOnInit() {
    console.log(this.event);
    this.eventForm = new FormGroup({
      nameInput: new FormControl(this.event.name),
      descriptionInput: new FormControl(this.event.description),
      locationInput: new FormControl(this.event.location),
      imgURLInput: new FormControl(this.event.imgURL),
      startDateInput: new FormControl(this.event.startDate),
      endDateInput: new FormControl(this.event.endDate),
      categoryInput: new FormControl(this.event.category)
    });
    this.getCategories();
  }


  saveOrUpdateEvent() {
    this.populateEventFromForm();

    if (this.event.id == null) {
      this.eventService.saveEvent(this.event)
        .subscribe((response) => {
          var savedEvent = response as Event;
          this.router.navigate(['/events/' + savedEvent.id]);
        });
    }
    if(this.event.id != null) {
     this.eventService.updateEvent(this.event.id,this.event)
        .subscribe((response) => {
          this.router.navigate(['/events/' + this.event.id]);
        });
    }
  }

  populateEventFromForm() {
    this.event.name = this.eventForm.value.nameInput;
    this.event.description = this.eventForm.value.descriptionInput;
    this.event.location = this.eventForm.value.locationInput;
    this.event.imgURL = this.eventForm.value.imgURLInput;
    this.event.startDate = this.eventForm.value.startDateInput;
    this.event.endDate = this.eventForm.value.endDateInput;
    this.event.category=this.eventForm.value.categoryInput;
  }

  getCategories(){
    this.categoryService.readCategories().subscribe(
      (response)=>{console.log(response);
        this.categories=response as Category[];
        console.log(this.categories);
      }
    );
  }


}
