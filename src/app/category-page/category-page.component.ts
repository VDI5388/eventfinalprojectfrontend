import {Component } from '@angular/core';
import {Category} from "src/app/model/category";
import {CategoryService} from "../category.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from '@angular/material/dialog';
import {ConfirmationDialogComponent} from '../confirmation-dialog/confirmation-dialog.component';
import {Event} from "src/app/model/event";

@Component({
  selector: 'app-category-page',
  templateUrl: './category-page.component.html',
  styleUrls: ['./category-page.component.css']
})


export class CategoryPageComponent {

  category: Category = new Category(
    null,
    null);


    updateCategoryUrl: string = "";

  route: ActivatedRoute;
  events:Event[]=[];

  constructor(private categoryService: CategoryService, route: ActivatedRoute, private router: Router, private dialog:MatDialog) {
    this.route = route;
  }

  ngOnInit() {
    const categoryid = this.route.snapshot.params["id"];
      //sintaxa pe a accesa parametrul id din cadrul url-ului

      this.updateCategoryUrl="/update-category/"+categoryid;

    this.categoryService.readCategory(categoryid).subscribe((response)=>
      {
      console.log(response)
      this.category=response as Category;
      },
      (error) =>
      {
        console.log(error);
        if (error.error == "There is no category with id: " + categoryid) {
          this.router.navigate(["/Page-not-found"])
        }
      });

    this.getEventsOfCategory();
  };




  public deleteCategory(categoryId:any){

    this.categoryService.deleteCategory(categoryId).subscribe(
      (response)=>{

        console.log("Category deleted!");

        this.router.navigate(["/categories"]);

      },error =>
        console.log(error)
    );
  }


  public openConfirmationDialog(){
    var dialogRef = this.dialog.open(ConfirmationDialogComponent);
    dialogRef.componentInstance.actionConfirmedEvent.subscribe(
      actionConfirmed =>{
        if(actionConfirmed ==true){
          this.deleteCategory(this.category.id);
        }
      });
  }



  navigateToEvent(event: Event){
    this.router.navigate([`/events/`+event.id])
}

getEventsOfCategory(){
    const categoryId = this.route.snapshot.params[`id`];
    this.categoryService.getEventsOfCategory(categoryId).subscribe(
      (response) => {
        this.events = response as Event[];
      }
    )

}

  }
