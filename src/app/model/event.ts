

import {Category} from "./category";

export class Event{

  id:number | null;
  name:string|null;
  description:string|null;
  location:string|null;
  startDate:Date|null;
  endDate:Date|null;
  imgURL:string|null;
  category:Category|null;


  constructor(id:number|null,name:string|null,description:string|null,location:string|null,startDate:Date|null,endDate:Date|null,imgURL:string|null,
              category:Category|null) {

    this.id=id;
    this.name=name;
    this.description=description;
    this.location=location;
    this.startDate=startDate;
    this.endDate=endDate;
    this.imgURL=imgURL;
    this.category=category;

  }


}
