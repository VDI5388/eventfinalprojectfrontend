import { Component } from '@angular/core';
import { Category } from '../model/category';
import { CategoryService } from '../category.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.css']
})
export class CategoriesPageComponent {

    categories: Category[] = [];

    constructor (private categoryService:CategoryService,
                 private router:Router){}

    ngOnInit(){

        this.categoryService.readCategories().subscribe(
          (response)=>
          {console.log(response);
          this.categories=response as Category[];
          console.log(this.categories);
        }
        );
    }

navigateToCategory(c:Category){
  this.router.navigate(["/categories/"+ c.id]);
}
}
