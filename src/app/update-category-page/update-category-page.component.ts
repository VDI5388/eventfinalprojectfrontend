import { Component } from '@angular/core';
import {Category} from "../model/category";
import {CategoryService} from "../category.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-update-category-page',
  templateUrl: './update-category-page.component.html',
  styleUrls: ['./update-category-page.component.css']
})
export class UpdateCategoryPageComponent {

  updateCategory : Category |null=null;

  constructor(private categoryService: CategoryService,
              private activeRoute:ActivatedRoute) {

    }

  ngOnInit(){

    const categoryId = this.activeRoute.snapshot.params["id"];
    this.categoryService.readCategory(categoryId).subscribe(
      (response) =>{
        this.updateCategory=response as Category;

      }
    )

  }
}
