import {Component, Input} from '@angular/core';
import {CategoryService} from "../category.service";
import {Router} from "@angular/router";
import {Category} from "../model/category";
import {FormControl, FormGroup} from "@angular/forms";


@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css']
})
export class CategoryFormComponent {

  constructor(private categoryService:CategoryService, private router:Router) {
  }

  @Input()
  category: Category = {
    id:null,
    name: "",
  }

  categoryForm:FormGroup = new FormGroup({
    nameInput: new FormControl(),
    });

  ngOnInit() {
    console.log(this.category);
    this.categoryForm = new FormGroup({
      nameInput: new FormControl(this.category.name),
        });
  }

  saveOrUpdateCategory(){
    this.populateCategoryFormForm();

    if (this.category.id==null){
      this.categoryService.saveCategory(this.category)
        .subscribe((response)=>{
          var savedCategory=response as Category;
          this.router.navigate([`/categories/`+ savedCategory.id]);
        })
    }

    if (this.category.id!=null){
      this.categoryService.updateCategory(this.category.id,this.category)
        .subscribe((response)=>{
          var savedCategory=response as Category;
          this.router.navigate([`/categories/`+ this.category.id]);
        })
    }

  }

  populateCategoryFormForm(){
    this.category.name=this.categoryForm.value.nameInput;
  }

}
